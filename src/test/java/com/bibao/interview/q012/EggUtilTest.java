package com.bibao.interview.q012;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 10/04/2019
 */
public class EggUtilTest {
	private static final Logger LOG = Logger.getLogger(EggUtilTest.class);
	
	private EggUtil util;
	
	@Before
	public void setUp() throws Exception {
		util = new EggUtil();
	}

	@Test
	public void testDrop() {
		assertEquals(10, util.drop(10, 1));
		assertEquals(4, util.drop(10, 2));
	}

	@Test
	public void testDropNonRecursive() {
		assertEquals(10, util.dropNonRecursive(10, 1));
		assertEquals(4, util.dropNonRecursive(10, 2));
		assertEquals(14, util.dropNonRecursive(100, 2));
		LOG.debug("Result: " + util.dropNonRecursive(1000, 4));
	}
}
