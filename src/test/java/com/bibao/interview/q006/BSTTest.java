package com.bibao.interview.q006;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

/**
 * @author Peter Xiong on 9/24/2019
 */
public class BSTTest {
	
	@Test
	public void testTraverseByLevel() {
		BST bst = createBST();
		List<Integer> result = bst.traverseByLevel();
		assertEquals("[4, 2, 6, 1, 3, 5, 8, 7]", result.toString());
	}

	@Test
	public void testPreOrderTraverse() {
		BST bst = createBST();
		List<Integer> result = bst.preOrderTraverse();
		assertEquals("[4, 2, 1, 3, 6, 5, 8, 7]", result.toString());
	}
	
	@Test
	public void testPreOrderTraverseNonRecursive() {
		BST bst = createBST();
		List<Integer> result = bst.preOrderTraverseNonRecursive();
		assertEquals("[4, 2, 1, 3, 6, 5, 8, 7]", result.toString());
	}
	
	@Test
	public void testInOrderTraverse() {
		BST bst = createBST();
		List<Integer> result = bst.inOrderTraverse();
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8]", result.toString());
	}
	
	@Test
	public void testInOrderTraverseNonRecursive() {
		BST bst = createBST();
		List<Integer> result = bst.inOrderTraverseNonRecursive();
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8]", result.toString());
	}
	
	@Test
	public void testPostOrderTraverse() {
		BST bst = createBST();
		List<Integer> result = bst.postOrderTraverse();
		assertEquals("[1, 3, 2, 5, 7, 8, 6, 4]", result.toString());
	}
	
	@Test
	public void testPostOrderTraverseNonRecursive() {
		BST bst = createBST();
		List<Integer> result = bst.postOrderTraverseNonRecursive();
		assertEquals("[1, 3, 2, 5, 7, 8, 6, 4]", result.toString());
	}
	
	private BST createBST() {
		int[] data = {4, 6, 8, 2, 5, 1, 3, 7};
		BST bst = new BST();
		for (int i=0; i<data.length; i++) bst.add(data[i]);
		return bst;
	}
}
