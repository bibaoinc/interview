package com.bibao.interview.q001;

import static org.junit.Assert.*;

import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/15/2019
 */
public class FibonacciTest {
	private static final Logger LOG = Logger.getLogger(FibonacciTest.class);
	private static Fibonacci fibo;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		fibo = new Fibonacci();
		LOG.debug("Size: " + Fibonacci.FIBO_VALUES.size());
	}

	@Test
	public void testGetRecursive() {
		Date start = new Date();
		for (int i=0; i<48; i++) {
			assertEquals(Fibonacci.FIBO_VALUES.get(i).longValue(), fibo.getRecursive(i));
		}
		Date end = new Date();
		LOG.debug("Running time for recursive: " + (end.getTime() - start.getTime()));
	}

	@Test
	public void testGetNonRecursive() {
		Date start = new Date();
		for (int i=0; i<Fibonacci.FIBO_VALUES.size(); i++) {
			assertEquals(Fibonacci.FIBO_VALUES.get(i).longValue(), fibo.getNonRecursive(i));
		}
		Date end = new Date();
		LOG.debug("Running time for non-recursive: " + (end.getTime() - start.getTime()));
	}
	
	@Test
	public void testGetByMath() {
		Date start = new Date();
		for (int i=0; i<=88; i++) {
			assertEquals(Fibonacci.FIBO_VALUES.get(i+1).longValue(), fibo.getByMath(i));
		}
		Date end = new Date();
		LOG.debug("Running time for fiboByMath: " + (end.getTime() - start.getTime()));
	}
}
