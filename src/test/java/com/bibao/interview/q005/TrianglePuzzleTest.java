package com.bibao.interview.q005;

import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class TrianglePuzzleTest {
	private static final int DATA_RANGE = 100;
	private static final Random RANDOM = new Random();
	
	@Test
	public void testSolve() {
		int[] data = {84, 49, 86, 11, 95, 63, 30, 6, 21, 52};
		TrianglePuzzle puzzle = new TrianglePuzzle(data);
		assertEquals(286, puzzle.solve());
	}

	@Test
	public void testSolveForRandomData() {
		int[] data = randomData(5);
		TrianglePuzzle puzzle = new TrianglePuzzle(data);
		puzzle.solve();
	}
	
	private int[] randomData(int level) {
		int n = level * (level + 1) / 2;
		int[] data = new int[n];
		for (int i=0; i<n; i++) {
			data[i] = RANDOM.nextInt(DATA_RANGE);
		}
		return data;
	}
}
