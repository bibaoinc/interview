package com.bibao.interview.q003;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class CoinMachineTest {
			
	@Test
	public void testFindMinCoinsByGreedy() {
		CoinMachine cm = new CoinMachine(1, 5, 10, 25);
		assertEquals(5, cm.findMinCoinsByGreedy(42));	// 42 = 25 + 10 + 5 + 2*1 (5 coins)
		cm = new CoinMachine(1, 10, 25);
		assertEquals(9, cm.findMinCoinsByGreedy(42));	// 42 = 25 + 10 + 7*1 (9 coins)
	}

	@Test
	public void testFindMinCoins() {
		CoinMachine cm = new CoinMachine(1, 5, 10, 25);
		// Correct result: one 25, one 10, one 5, two 1 (5 coins)
		assertEquals(5, cm.findMinCoins(42));	
		cm = new CoinMachine(1, 10, 25);
		// Correct result: four 10, two 1 (6 coins)
		assertEquals(6, cm.findMinCoins(42));			
	}
	
}
