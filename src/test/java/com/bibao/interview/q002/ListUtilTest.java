package com.bibao.interview.q002;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Xiong on 9/16/2019
 */
public class ListUtilTest {
	private ListUtil util;
	
	@Before
	public void setUp() {
		util = new ListUtil();
	}

	@Test
	public void testGetMax() {
		List<Integer> list = Arrays.asList(15, -3, 17, 28, 5, 41, -2, -2, 0);
		assertEquals(41, util.getMax(list).intValue());
	}

	@Test(expected=NoElementException.class)
	public void testGetMaxFailCase1() {
		util.getMax(null);
	}
	
	@Test(expected=NoElementException.class)
	public void testGetMaxFailCase2() {
		util.getMax(new ArrayList<>());
	}
	
	@Test(expected=NoElementException.class)
	public void testGetMaxFailCase3() {
		List<Integer> list= Arrays.asList(null, null);
		util.getMax(list);
	}
}
