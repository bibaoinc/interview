package com.bibao.interview.q002;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ContractTest {
	private static final Logger LOG = Logger.getLogger(ContractTest.class);
	
	private ObjectMapper mapper;
	
	@Before
	public void setUp() {
		mapper = new ObjectMapper();
	}

	@Test
	public void testFullContract() {
		Contract contract = new Contract();
		contract.setId(10);
		contract.setValue(100);
		contract.setActive(true);
		contract.setSold(false);
		contract.setDescription("Full Contract");
		try {
			String message = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contract);
			assertTrue(message.contains("id"));
			assertTrue(message.contains("value"));
			assertTrue(message.contains("active"));
			assertTrue(message.contains("sold"));
			assertTrue(message.contains("description"));
			LOG.debug("Contract: \n" + message);
		} catch (Exception e) {
			LOG.error("Error: " + e.getMessage());
		}
	}

	@Test
	public void testPartialContract() {
		Contract contract = new Contract();
		contract.setId(10);
		contract.setDescription("Partial Contract");
		try {
			String message = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(contract);
			assertTrue(message.contains("id"));
			assertFalse(message.contains("value"));
			assertFalse(message.contains("active"));
			assertTrue(message.contains("sold"));
			assertTrue(message.contains("description"));
			LOG.debug("Contract: \n" + message);
		} catch (Exception e) {
			LOG.error("Error: " + e.getMessage());
		}
	}
}
