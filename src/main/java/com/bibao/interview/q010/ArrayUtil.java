package com.bibao.interview.q010;

import org.apache.log4j.Logger;

/**
 * @author Peter Xiong on 9/28/2019
 */
public class ArrayUtil {
	/************************************************************
	 * Description: Find a sub-array of an array of integers	*
	 * 				that has the maximum summation				*
	 ************************************************************/
	private static final Logger LOG = Logger.getLogger(ArrayUtil.class);
	
	public int maxSumOfSubArrayInBrutalForce(int... array) {
		int sum = Integer.MIN_VALUE;
		int startIndex = -1;
		int endIndex = -1;
		int size = array.length;
		int n = 1;				// represents the length of the array
		while (n<=size) {		// Running Time: O(n^2)
			// Add the first n items
			int currentSum = 0;
			for (int i=0; i<n; i++) {
				currentSum += array[i];
			}
			if (currentSum>sum) {
				sum = currentSum;
				startIndex = 0;
				endIndex = n-1;
			}
			// Move to the rest items without making a summation again. 
			for (int k=n; k<size; k++) {
				currentSum = currentSum + array[k] - array[k-n];
				if (currentSum>sum) {
					sum = currentSum;
					startIndex = k - n + 1;
					endIndex = k;
				}
			}
			n++;
		}
		displayLog(sum, startIndex, endIndex);
		return sum;
	}
	
	public int maxSumOfSubArray(int... array) {
		int sum = Integer.MIN_VALUE;
		int startIndex = -1;
		int endIndex = -1;
		int size = array.length;
		
		// Running Time: O(n)
		if (isAllNegative(array)) {
			sum = array[0];
			startIndex = 0;
			endIndex = 0;
			for (int i=1; i<size; i++) {
				if (array[i]>sum) {
					sum = array[i];
					startIndex = i;
					endIndex = i;
				}
			}
			displayLog(sum, startIndex, endIndex);
			return sum;
		}
		// Starting from the first positive integer, add following integers until sum is negative,
		// then record this block and repeat this procedure
		int currentIndex = 0;
		while (currentIndex < size) {
			while (currentIndex<size && array[currentIndex]<0) currentIndex++;
			if (currentIndex>=size) break;
			int currentSum = array[currentIndex];
			int currentStartIndex = currentIndex;
			int currentEndIndex = currentIndex;
			int movingSum = currentSum;
			currentIndex++;
			while (currentIndex<size) {
				movingSum += array[currentIndex];
				if (movingSum>currentSum) {
					currentSum = movingSum;
					currentEndIndex = currentIndex;
				}
				if (movingSum<=0) break;
				currentIndex++;
			}
			if (currentSum>sum) {
				sum = currentSum;
				startIndex = currentStartIndex;
				endIndex = currentEndIndex;
			}
		}
		displayLog(sum, startIndex, endIndex);
		return sum;
	}
	
	private boolean isAllNegative(int... array) {
		for (int i=0; i<array.length; i++) {
			if (array[i]>0) return false;
		}
		return true;
	}
	
	private void displayLog(int sum, int startIndex, int endIndex) {
		LOG.debug("Maximum Sum " + sum + " starts from index " + startIndex + " to index " + endIndex);
	}
}
