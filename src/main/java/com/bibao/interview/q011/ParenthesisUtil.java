package com.bibao.interview.q011;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Peter Xiong on 10/04/2019
 */
public class ParenthesisUtil {
	/************************************************************
	 * Description: Given integer n, find all possible 			*
	 * 				combinations of n parenthesis				*
	 ************************************************************/
	
	public Set<String> findAllParens(int n) {
		Set<String> result = new HashSet<>();
		parensRecursive(n, 0, "", result);
		return result;
	}
	
	private void parensRecursive(int left, int right, String current, Set<String> result) {
		if (left==0 && right==0) {
			result.add(current);
			return;
		}
		if (left>0) {
			parensRecursive(left-1, right+1, current + "(", result);
		}
		if (right>0) {
			parensRecursive(left, right-1, current+")", result);
		}
	}
	
	public Set<String> findAllParensNonRecursive(int n) {
		Map<Integer, Set<String>> map = init();
		for (int i=3; i<=n; i++) {
			Set<String> currentResult = new HashSet<>();
			// Obtained from previous set
			for (String s: map.get(i-1)) {
				currentResult.add(s + "()");
				currentResult.add("()" + s);
				currentResult.add("(" + s + ")");
			}
			for (int j=2; j<=i/2; j++) {
				Set<String> first = map.get(j);
				Set<String> second = map.get(i-j);
				for (String s1: first) {
					for (String s2: second) {
						currentResult.add(s1 + s2);
						currentResult.add(s2 + s1);
					}
				}
			}
			map.put(i, currentResult);
		}
		return map.get(n);
	}
	
	private Map<Integer, Set<String>> init() {
		Map<Integer, Set<String>> map = new HashMap<>();
		Set<String> parens1 = new HashSet<>(Arrays.asList("()"));
		Set<String> parens2 = new HashSet<>(Arrays.asList("()()", "(())"));
		map.put(1, parens1);
		map.put(2, parens2);
		return map;
	}
}
