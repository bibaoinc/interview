package com.bibao.interview.q012;

/**
 * @author Peter Xiong on 10/04/2019
 */
public class EggUtil {
	/************************************************************
	 * Description: Given n floors and k eggs, find the minimum	*
	 * 				number of drops of eggs to get the lowest 	*
	 * 				floor that breaks the egg.					*
	 ************************************************************/
	
	public int drop(int n, int k) {		// n floors, k eggs
		if (k==1 || (n<=1)) return n;
		int result = Integer.MAX_VALUE;
		for (int i=1; i<=n; i++) {
			int currentResult = Math.max(1 + drop(n-i, k), 1 + drop(i-1, k-1));
			if (currentResult<result) result = currentResult;
		}
		return result;
	}
	
	public int dropNonRecursive(int n, int k) {
		int[][] array = new int[k+1][n+1];
		for (int i=1; i<=n; i++) array[1][i] = i;
		for (int i=1; i<=k; i++) array[i][1] = 1;
		for (int i=2; i<=k; i++) {		
			for (int j=2; j<=n; j++) {
				// Try drop 1 egg from the first floor to the j-th floor and the find the min number of drops
				array[i][j] = Integer.MAX_VALUE;
				for (int m=1; m<=j; m++) {
					// If the egg is broken, then try the lower m-1 floors with i-1 eggs which is array[i-1][m-1]
					// If the egg is not broken, then try the upper j-m floors with i eggs which is array[i][j-m]
					int result = Math.max(1 + array[i][j-m], 1 + array[i-1][m-1]);
					if (result<array[i][j]) array[i][j] = result;
				}
			}
		}
		return array[k][n];
	}
}
