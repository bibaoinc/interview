Given the following triangle graph, find the path that has the maximum summation.
                        84
                       /  \
                      49  86
                     /  \/  \
                    11  95  63
                   /  \/  \/  \
                  30  6   21  52
                  
Solution:
Path: 84 -> 86 -> 95 -> 21
Summation: 286
