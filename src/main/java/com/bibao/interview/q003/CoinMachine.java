package com.bibao.interview.q003;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**
 * @author Peter Xiong on 9/22/2019
 */
public class CoinMachine {
	/****************************************************************
	 * Description: Find minimum number of coins for an input cents	*
	 ****************************************************************/
	
	private static final Logger LOG = Logger.getLogger(CoinMachine.class);
	
	private int[] coins;
	
	public CoinMachine() {}
	
	public CoinMachine(int... coins) {
		this.coins = coins;
		Arrays.sort(coins);
		this.coins[0] = 1;				// Always guarantee 1 cent coin exists
	}
	
	public int findMinCoinsByGreedy(int cents) {
		int numOfCoins = 0;
		int n = coins.length;
		int currentCents = cents;
		for (int i=n-1; i>=0; i--) {
			numOfCoins += currentCents / coins[i];
			currentCents = currentCents % coins[i];
		}
		return numOfCoins;
	}
	
	public int findMinCoins(int cents) {
		int m = coins.length + 1;
		int n = cents + 1;
		int[][] buffer = new int[m][n];
		for (int col = 1; col<n; col++) buffer[1][col] = col;		// For 1 cent coin row
		for (int row=2; row<m; row++) {
			for (int col=1; col<n; col++) {
				if (col>=coins[row-1]) {
					buffer[row][col] = Math.min(buffer[row-1][col], buffer[row][col-coins[row-1]] + 1);
				} else {
					buffer[row][col] = buffer[row-1][col];
				}
			}
		}
		display(buffer, m, n);
		return buffer[m-1][n-1];
	}

	public int[] getCoins() {
		return coins;
	}

	public void setCoins(int[] coins) {
		this.coins = coins;
		Arrays.sort(this.coins);
		this.coins[0] = 1;
	}
	
	public void display(int[][] buffer, int row, int col) {
		for (int i=0; i<row; i++) {
			StringBuilder builder = new StringBuilder();
			for (int j=0; j<col; j++) {
				builder.append(buffer[i][j] + " ");
			}
			LOG.debug(builder.toString());
		}
	}
}
