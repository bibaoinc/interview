package com.bibao.interview.q002;

/**
 * @author Peter Xiong on 9/16/2019
 */
public class NoElementException extends RuntimeException {
	private static final long serialVersionUID = -714150921808774335L;

	public NoElementException() {
		super();
	}
	
	public NoElementException(String errorMsg) {
		super(errorMsg);
	}
}
