package com.bibao.interview.q001;

/**
 * @author Peter Xiong on 9/16/2019
 */
public class InvalidInputException extends RuntimeException {
	private static final long serialVersionUID = 1779630391778869953L;

	public InvalidInputException() {
		super();
	}
	
	public InvalidInputException(String errorMsg) {
		super(errorMsg);
	}
}
